-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2016 at 02:45 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birth_day`
--

CREATE TABLE `birth_day` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `birth_day` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birth_day`
--

INSERT INTO `birth_day` (`id`, `name`, `birth_day`) VALUES
(1, 'Sharmin', '1985-05-20'),
(2, 'dd', '1985-05-25'),
(3, 'Nafisa', '0000-00-00'),
(4, 'farahah', '1955-05-20'),
(5, 'rehan', '1982-02-13'),
(6, 'shanta', '1965-04-02'),
(7, 'rima', '1985-02-03'),
(8, 'hero', '1985-02-03'),
(9, 'jamal', '1982-02-13'),
(10, 'jibon', '1985-05-20'),
(11, 'titu', '1982-02-13'),
(12, 'yakub', '1985-05-20'),
(13, 'jihan', '1982-02-13'),
(14, 'Jahangir', '1985-05-20'),
(15, 'lili', '1985-02-03'),
(16, 'pinky', '1985-05-25'),
(17, 'kabery', '1985-05-20'),
(18, 'papri', '1982-02-13'),
(19, 'Nibraz', '0000-00-00'),
(20, 'cccc', '1982-07-15'),
(21, 'dddsd', '2016-11-08');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`) VALUES
(1, 'Sesh Bela', 'Srikanta'),
(3, 'ccc', 'ccc');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `city_name` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city_name`) VALUES
(1, 'shahjahan', 'Rajshahi'),
(2, 'ddd', 'Dhaka'),
(3, 'fdsfsd', 'Ctg'),
(6, 'fiza', 'Barishal'),
(7, 'ddd', 'Rajshahi'),
(8, 'kabery', ' Barishal'),
(9, 'Rima', ' Khulna'),
(10, 'rabeya', 'Rajshahi'),
(11, 'Sihab', 'Barishal');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email_address` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email_address`) VALUES
(1, 'daina', 'far@yahoo.com'),
(2, 'saddam', 'saddam.jbl@yahoo.com'),
(3, 'shahana', 'shah@gmail.com'),
(4, 'Tina', 'tina@yahoo.com'),
(6, 'Hamida', 'hamida@gmail.com'),
(7, 'shamim', 'shamim.hassan@gmail.com'),
(8, 'faa', 'saddam.jbl@yahoo.com'),
(9, 'Farheen', 'shah@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `gender_name` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender_name`) VALUES
(1, 'fff', 'female'),
(3, 'Farhana', 'female'),
(4, 'jerin', 'female'),
(5, 'jahan', 'female'),
(6, 'Saddam', 'male'),
(7, 'Salam', 'male'),
(8, 'Ayesha', 'female'),
(9, 'xxxx', 'female');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `hobby_name` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobby_name`) VALUES
(7, 'Fariha', 'drawing,dancing,'),
(8, 'Shahana', 'drawing,singing,'),
(10, 'Koli', 'on,'),
(11, 'Sima', 'on,on,'),
(13, 'frgr', 'Singing,Dancing,'),
(14, 'Jana', 'Drawing,Gardening,Singing,Dancing,'),
(15, 'ssss', 'Gardening,Singing,'),
(16, '', 'Drawing,Gardening,Dancing,'),
(17, 'karim', 'Gardening,Singing,'),
(18, 'hamid', 'Drawing,Dancing,'),
(30, 'Sneha', 'Drawing,Gardening'),
(33, 'Habiba', 'Gardening,Singing,Dancing');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `profile_pic` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_pic`) VALUES
(12, 'koli', '/xampp/htdocs/img/14788693802.jpg'),
(13, 'hami', '/xampp/htdocs/img/14788694233.jpg'),
(15, 'Nafisa', '/xampp/htdocs/img/1478870565windows product key.jpg'),
(16, 'Guddu', '/xampp/htdocs/img/1478870655windows product key.jpg'),
(18, 'jimy', '/xampp/htdocs/img/1478876138subform.jpg'),
(19, 'sima', '/xampp/htdocs/img/1478876192subform.jpg'),
(20, 'rokon', '/xampp/htdocs/img/1478878619subform.jpg'),
(21, 'fff', '/xampp/htdocs/img/1478892713secondarytile.png'),
(22, 'Abrar', '/xampp/htdocs/img/1479062528subform.jpg'),
(23, 'Raina', '1479062603/xampp/htdocs/img/windows product key.jpg'),
(24, 'Jalal', '/xampp/htdocs/img/1479062745a.jpg'),
(25, 'Kabir', '/xampp/htdocs/Labexam7_Farhana_SEIP146905_b36/image/1479198407a.jpg'),
(30, 'shakila', 'xampp/htdocs/Farhana_SEIP146905_b36_Session26/image/14792292221477135320images (2).jpg'),
(31, 'laky', 'xampp/htdocs/Farhana_SEIP146905_b36_Session26/image/1479232869hobby.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(11) NOT NULL,
  `company_name` varchar(111) NOT NULL,
  `company_summary` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `company_name`, `company_summary`) VALUES
(1, 'MGH Company', 'Its a multinational company'),
(2, 'Uttara Motors', 'Auto Industries Ltd.'),
(3, 'DeshIT Ltd.', 'Its a IT firm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birth_day`
--
ALTER TABLE `birth_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birth_day`
--
ALTER TABLE `birth_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
