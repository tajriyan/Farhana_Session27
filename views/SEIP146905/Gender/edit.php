
<?php
require_once("../../../vendor/autoload.php");
use App\Gender\Gender;
$objBookTtitle =new Gender();
$objBookTtitle->setData($_GET);
$oneData=$objBookTtitle->view();

?>
<html>

<head>
    <title></title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/asset/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/asset/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/asset/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/asset/css/style.css">
    <style type="text/css">
        .header {
            color: #3c3c3c;
            font-size: 27px;
            padding: 10px;
        }

        .bigicon {
            font-size: 35px;
            color: #2b669a;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $("p").delay(2000).fadeOut("slow")
        });
    </script>
</head>

<body>
<p>
    <?php
    use App\Message\Message;
    echo Message::message();
    ?>
</p>
<!-- Top content -->
<div class="top-content">

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Add Name And Gender</h3>
                    <h4>Enter Name And Gender:</h4>
                </div>
                <div class="form-top-right">
                    <i class="fa fa-child"></i>
                </div>
            </div>
            <div class="form-bottom">
                <form role="form" action="update.php" method="post" class="login-form">
                    <input type="hidden" name="id" value="<?php echo $oneData->id?>">
                    <div class="form-group">
                        <label class="sr-only" for="form-username"> Name</label>
                        <input type="text" name="name" value="<?php echo $oneData->name?>" class="form-username form-control" id="book_title">
                    </div>
                    <div class="form-group">
                        <label>Gender......</label><br>
                        <input type="radio" name="gender_name" value="male" <?php echo ($oneData->gender_name=='male')?'checked':'' ?> > Male<br>
                        <input type="radio" name="gender_name" value="female" <?php echo ($oneData->gender_name=='female')?'checked':'' ?>> Female<br>
                    </div>
                    <button type="submit" class="btn">Update</button>
                </form>
            </div>
        </div>

    </div>

    <!-- Javascript -->
    <script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
    <script src="../../../resource/assets/js/scripts.js"></script>

    <!--[if lt IE 10]>
    <script src="../../../resource/assets/js/placeholder.js"></script>
    <![endif]-->

</body>

</html>