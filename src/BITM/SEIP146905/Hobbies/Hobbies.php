<?php
namespace App\Hobbies;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
class Hobbies extends DB
{
    public $id;
    public $name;
    public $hobby_name;
    public function __construct()
    {
        parent:: __construct();
    }
    public function setData($postVariable=null)
    {

        if(array_key_exists("id",$postVariable))
        {
            $this->id =        $postVariable['id'];
        }
        if(array_key_exists("name",$postVariable))
        {
            $this->name =        $postVariable['name'];
        }
        if(array_key_exists("hobby_name",$postVariable))
        {
            $this->hobby_name =        $postVariable['hobby_name'];
        }
    }
    public function store(){
        $chk = implode(",",$this->hobby_name);
        for($i = 0; $i < sizeof($chk); $i++)
         /*   $checkbox1=$this->hobby_name;
        $chk="";
        foreach($checkbox1 as $chk1)
        {
            $chk .= $chk1." ";
        }*/
        $arrayData=array($this->name,$chk);
        $sql="insert into hobby(name,hobby_name)VALUES (?,?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrayData);
        if($result)
            Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");

        Utility::redirect('create.php');
    }
    public  function index()
    {
        $STH = $this->conn->query("SELECT * from hobby ORDER BY id DESC");

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    public function view(){

        $sql = 'SELECT * from hobby where id='.$this->id;

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }
    public function update()
    {
        $chk = implode(",",$this->hobby_name);
        for($i = 0; $i < sizeof($chk); $i++)
        $arrayData=array($this->name,$chk);
        $sql="update hobby set name=?, hobby_name=? where id=".$this->id;
        $STH = $this->conn->prepare($sql);
        $STH->execute($arrayData);

        Utility::redirect('index.php');

    }// end
    public function delete(){

        $sql = "Delete from hobby where id=".$this->id;

        $STH = $this->conn->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()
}
?>

