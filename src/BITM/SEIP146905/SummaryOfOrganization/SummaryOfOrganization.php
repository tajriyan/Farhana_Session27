<?php
namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
class SummaryOfOrganization extends DB{
    public $id;
    public $company_name;
    public $company_summary;
    public function __construct()
    {
        parent:: __construct();
    }
    public function setData($postVariable=null)
    {

        if(array_key_exists("id",$postVariable))
        {
            $this->id =        $postVariable['id'];
        }
        if(array_key_exists("company_name",$postVariable))
        {
            $this->company_name =        $postVariable['company_name'];
        }
        if(array_key_exists("company_summary",$postVariable))
        {
            $this->company_summary =        $postVariable['company_summary'];
        }
    }
    public function store()
    {
        $arrayData=array($this->company_name,$this->company_summary);
        $sql="insert into summary_of_organization(company_name,company_summary)VALUES (?,?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrayData);
        if($result)
            Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");
        Utility::redirect('create.php');
    }
    public  function index()
    {
        $STH = $this->conn->query("SELECT * from summary_of_organization ORDER BY id DESC");

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }

    public function view(){

        $sql = 'SELECT * from summary_of_organization where id='.$this->id;

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }
    public function update()
    {
        $arrayData=array($this->company_name,$this->company_summary);
        $sql="update summary_of_organization set company_name=?, company_summary=? where id=".$this->id;
        $STH = $this->conn->prepare($sql);
        $STH->execute($arrayData);

        Utility::redirect('index.php');

    }// end
    public function delete(){

        $sql = "Delete from summary_of_organization where id=".$this->id;

        $STH = $this->conn->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()
}
?>

