<?php
namespace App\Gender;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
class Gender extends DB{
    public $id;
    public $name;
    public $gender_name;
    public function __construct()
    {
        parent:: __construct();
    }
    public function setData($postVariable=null)
    {

        if(array_key_exists("id",$postVariable))
        {
            $this->id =        $postVariable['id'];
        }
        if(array_key_exists("name",$postVariable))
        {
            $this->name =        $postVariable['name'];
        }
        if(array_key_exists("gender_name",$postVariable))
        {
            $this->gender_name =        $postVariable['gender_name'];
        }
    }
    public function store(){
        $arrayData=array($this->name,$this->gender_name);
        $sql="insert into gender(name,gender_name)VALUES (?,?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrayData);
        if($result)
            Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");

        Utility::redirect('create.php');
    }
    public  function index()
    {
        $STH = $this->conn->query("SELECT * from gender ORDER BY id DESC");

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    public function view(){

        $sql = 'SELECT * from gender where id='.$this->id;

        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrOneData  = $STH->fetch();
        return $arrOneData;
    }
    public function update()
    {
        $arrayData=array($this->name,$this->gender_name);
        $sql="update gender set name=?, gender_name=? where id=".$this->id;
        $STH = $this->conn->prepare($sql);
        $STH->execute($arrayData);

        Utility::redirect('index.php');

    }// end
    public function delete(){

        $sql = "Delete from gender where id=".$this->id;

        $STH = $this->conn->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()
}
?>
